from email import message
import os
import requests
from django.shortcuts import render
from dotenv import load_dotenv

# Create your views here.

load_dotenv()
def sendSms(phone_number, transaction_id, amount):
    smsEndpoint=os.getenv("SMS_ENDPOINT")
    headers={
        "Content-Type":"application/json",
        "X-API-KEY":os.getenv("API_KEY")
    }
    data={
        "messages":[
            {
                "message":"Thank you. We have received your payment of {}, Ref no {}. Thank you for doing business with us.".format(amount, transaction_id),
                "message_type":1,
                "req_type":1,
                "recipient":phone_number,
                "external_id":"abc"
            }
        ],
        "profile_code":"{}".format(os.getenv("PROFILE_CODE")),
        "dlr_callback_url":"http://127.0.0.1/api/v1/payments/validate/"
    }

    response=requests.post(smsEndpoint, json=data, headers=headers)

    print(response)

    return response
