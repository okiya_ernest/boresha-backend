from django.urls import path
from .views import MyObtainTokenPairView,RegisterView,UserRetrieveUpdateView,AllUsersView
from rest_framework_simplejwt.views import TokenRefreshView


urlpatterns = [
  path('login/', MyObtainTokenPairView.as_view(), name='token_obtain_pair'),
  path('login/refresh', TokenRefreshView.as_view(), name='token_refresh'),
  path('register/', RegisterView.as_view(), name='register'),
  path('users/', AllUsersView.as_view(), name='all_users'),
  path('users/<uuid:url>/', UserRetrieveUpdateView.as_view(), name='edit_user')
]