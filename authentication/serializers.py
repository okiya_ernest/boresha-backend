from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from rest_framework.response import Response
from django.contrib.auth.password_validation import validate_password
from .models import User
import uuid
import datetime
import random

class MyTokenObtainPairSerializer(TokenObtainPairSerializer):
  @classmethod
  def get_token(cls, user):
    token = super(MyTokenObtainPairSerializer,cls).get_token(user)

    #add custom claims
    return token

class RegisterSerializer(serializers.ModelSerializer):
  # email = serializers.EmailField(required = True, validators = [UniqueValidator(queryset=User.objects.all())])
  password = serializers.CharField(write_only = True, required=False)
  username=serializers.CharField(required=False)


  class Meta:
    model = User
    exclude=['date_updated','visibility']
  
  def create(self, validated_data):
    full_name=validated_data['full_name']
    ##Generate a unique username
    lower = 'abcdefghijklmnopqrstuvwxyz'
    upper = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    number = '1234567890'
    length = 7
    all = lower+upper+number
    randomString=''.join(random.sample(all, length))
    username = full_name +'_'+randomString
    user = User.objects.create(
      username = username,
      full_name = validated_data['full_name'],
      email = validated_data['email'],
      url = uuid.uuid4(),
    )
    # user.set_password(validated_data['password'])
    user.save()

    return user
# groups decorator

class UserRetrieveUpdateSerializer(serializers.ModelSerializer):
  class Meta:
    model = User
    fields = ['full_name','email', 'url']
    read_only_fields = ('id',)

  def update(self,instance,validated_data):
    instance.full_name = validated_data.get('full_name')
    instance.email = validated_data.get('email')
    instance.date_updated = datetime.datetime.now()
    instance.save()
    return instance

  


