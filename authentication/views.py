from functools import partial
from django.core.exceptions import ObjectDoesNotExist
from django.http.response import Http404
from django.shortcuts import get_object_or_404, render
from rest_framework import generics, status
from rest_framework.validators import UniqueValidator
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import permissions, decorators

# from base import serializer
from .serializers import MyTokenObtainPairSerializer,RegisterSerializer,UserRetrieveUpdateSerializer
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework_simplejwt.views import TokenObtainPairView
from rest_framework import generics
from .models import User
# from .permissions import IsStaff


# Create your views here.

class MyObtainTokenPairView(TokenObtainPairView):
  permission_classes = (AllowAny,)
  serializer_class = MyTokenObtainPairSerializer

class RegisterView(generics.CreateAPIView):
  queryset = User.objects.all()
  permission_classes = (AllowAny,)
  serializer_class = RegisterSerializer

class UserRetrieveUpdateView(APIView):
  def get(self, request, url, format = None):
    user = get_object_or_404(User, url =url)
    serializer = UserRetrieveUpdateSerializer(user)
    return Response(serializer.data)

  def put(self,request,url, format = None):
    user = get_object_or_404(User, url =url)
    serializer = UserRetrieveUpdateSerializer(user, data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

  def delete(self,request,pk, format = None):
    user = get_object_or_404(User, pk =pk)
    user.visibility = 0
    user.save()
    return Response({'message': 'User deleted successfully'})

class AllUsersView(APIView):
  permission_classes=[AllowAny]
  def get(self,request):
    all_users = User.objects.all()
    serializer = UserRetrieveUpdateSerializer(all_users, many = True)
    return Response({"status":1,"data":serializer.data})