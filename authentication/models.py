from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.

class User(AbstractUser):

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS=['full_name', 'username']

    full_name = models.CharField(max_length=255)
    email = models.EmailField(unique=True)
    is_staff=models.BooleanField(null=False, default=True)
    date_created=models.DateTimeField(auto_now_add=True)
    date_updated=models.DateTimeField(null=True)
    visibility=models.BooleanField(null=False, default=True)
    url=models.UUIDField(null=True)

    def __str__(self):
        return self.full_name