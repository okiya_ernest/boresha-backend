import datetime
import requests
import os
import json
from django.shortcuts import render
from dotenv import load_dotenv
from requests.auth import HTTPBasicAuth
from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from notifications.views import sendSms

from payments.models import Transaction
from payments.serializers import TransactionSerializers

from django.db.models import Sum, Q

# Create your views here.
load_dotenv()

# authiroziation API
def generateKeys():
    try:
        response=requests.get(
                    "{}/oauth/v1/generate?grant_type=client_credentials".format(os.getenv("MPESA_TEST_DOMAIN")), 
                    auth=HTTPBasicAuth(os.getenv("CUSTOMER_KEY"), os.getenv("CUSTOMER_SECRET"))
                    )
        formattedResponse=dict(json.loads(response.text))
        return formattedResponse['access_token']
    except Exception as e:
        return HttpResponse(e)

@csrf_exempt
def c2bSimulate(request):
    try:
        mpesaEndpoint="{}/mpesa/c2b/v1/simulate".format(os.getenv("MPESA_TEST_DOMAIN"))
        requestBody={
            "ShortCode":"{}".format(os.getenv("MPESA_SHORTCODE")),
            "CommandID":"CustomerPayBillOnline",
            "Amount":100,
            "Msisdn":"254700221171",
            "BillRefNumber":"1000"
        }
        headers={
            "Authorization":"Bearer {}".format(generateKeys())
        }
        response=requests.post(mpesaEndpoint, json=requestBody, headers=headers)
        formattedResponse=dict(json.loads(response.text))
        return JsonResponse(formattedResponse)

    except Exception as e:
        return HttpResponse(e)
    
@csrf_exempt
def confirmationURL(request):
    try:
    # mpesa_payment=request.data
        print(request)
        mpesa_body =request.body.decode('utf-8')
        mpesa_payment = json.loads(mpesa_body)
        payment = Transaction(
            first_name=mpesa_payment['FirstName'],
            last_name=mpesa_payment['LastName'],
            middle_name=mpesa_payment['MiddleName'],
            transaction_id=mpesa_payment['TransID'],
            phone_number=mpesa_payment['MSISDN'],
            amount=mpesa_payment['TransAmount'],
            reference=mpesa_payment['BillRefNumber'],
            type=mpesa_payment['TransactionType'],
        )
        payment.save()

        sendSms(mpesa_payment['MSISDN'], mpesa_payment['TransID'], mpesa_payment['TransAmount'])
        response={
            "ResultCode":1,
            "ResultDesc":"Accepted"
        }
        return JsonResponse(dict(response))
    except Exception as e:
        response ={
            "status":0,
            "data":{
                "message":"Error occurred. {}".format(e)
            }
        }

        return JsonResponse(response)
@csrf_exempt
def validationURL(request):
    print(request)
    response={
        "ResultCode":1,
        "ResultDesc":"Accepted"
    }
    return JsonResponse(dict(response))
    
def registerURLs(request):
    try:
        mpesaEndpoint="{}/mpesa/c2b/v1/registerurl".format(os.getenv("MPESA_TEST_DOMAIN"))
        requestBody={
            "ShortCode": "{}".format(os.getenv("MPESA_SHORTCODE")),
            "ResponseType": "Completed",
            "ConfirmationURL": "https://8061-196-207-177-94.ngrok.io/api/v1/payments/confirm/",
            "ValidationURL": "https://8061-196-207-177-94.ngrok.io/api/v1/payments/validate/"
        }
        headers={
            "Authorization":"Bearer {}".format(generateKeys())
        }
        response=requests.post(mpesaEndpoint, json=requestBody, headers=headers)
        formattedResponse=dict(json.loads(response.text))
        return JsonResponse(formattedResponse)

    except Exception as e:
        return HttpResponse(e)



def getTransactions(request):
    try:
        transactions=Transaction.objects.all().order_by("-id")

        serializedTransactions=TransactionSerializers(instance=transactions,many=True)

        response ={
            "status":1,
            "data":{
                "transactions":serializedTransactions.data
            }
        }

        return JsonResponse(response)
    except Exception as e:
        response ={
            "status":0,
            "data":{
                "message":"Error occurred. {}".format(e)
            }
        }

        return JsonResponse(response)

def getDashboardData(request):
    try:
        # get amount gotten today
        dateToday=datetime.datetime.date(datetime.datetime.now())
        totalAmountEarned=Transaction.objects.filter(date_added__range=(dateToday, dateToday)).aggregate(totalAmountEarned=Sum('amount'))

        # get last 5 transactions of the day
        lastFiveTransactions=Transaction.objects.filter(date_added__range=(dateToday, dateToday)).order_by("-id")[:5]
        serializedTransactions=TransactionSerializers(lastFiveTransactions, many=True)

        response={
            "status":1,
            "data":{
                "totalAmountEarnedToday":totalAmountEarned['totalAmountEarned'],
                "lastFiveTransactions":serializedTransactions.data
            }
        }

        return JsonResponse(response)

    except Exception as e:
        response ={
            "status":0,
            "data":{
                "message":"Error occurred. {}".format(e)
            }
        }

        return JsonResponse(response)




