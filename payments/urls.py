from django.urls import path
from .views import c2bSimulate, confirmationURL, getDashboardData, getTransactions, validationURL, registerURLs, generateKeys


urlpatterns=[
    path('confirm/', confirmationURL, name="confirm"),
    path('getTransactions/', getTransactions, name="getTransactions"),
    path('validate/', validationURL, name="validate"),
    path('registerUrls', registerURLs, name="registerUrls"),
    path('c2bSimulate/', c2bSimulate, name="c2bSimulate"),
    path('genKeys', generateKeys, name="genKeys"),
    path('getDashboardData/', getDashboardData, name="getDashboardData"),
]