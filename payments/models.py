from django.db import models

# Create your models here.
class Transaction(models.Model):
    first_name=models.CharField(max_length=255)
    last_name=models.CharField(max_length=255)
    middle_name=models.CharField(max_length=255)
    transaction_id=models.CharField(max_length=255)
    phone_number=models.CharField(max_length=255)
    amount=models.DecimalField(max_digits=10, decimal_places=2)
    reference=models.CharField(max_length=255)
    type=models.CharField(max_length=255)
    date_added=models.DateField(auto_now_add=True)
    created_date_time=models.DateTimeField(auto_now_add=True)